module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      boxShadow: {
        '3xl': '0 25px 60px 10px rgb(0 0 0 / 30%)',
      },
      borderWidth: {
        '1': '1px'
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}

import { Chat24, Help24, Notification24, Search24, User24, WatsonHealthContourFinding24, Home20, Activity20, Analytics20, Subtract20, Add20 } from '@carbon/icons-react';

export default [
    {
        title: 'Dashboard',
        toggle: true,
        children: [
            {
                logo: (props) => <Home20 {...props} />,
                title: 'Dashboard',
                link: '#'
            },
            {
                logo: (props) => <Activity20 {...props} />,
                title: 'Platform analytics',
                link: '#',
                des: {
                    color: '#D97706',
                    text: '!'
                },
                children: [
                    {
                        title: 'Settings',
                        des: {
                            color: '#ff6057',
                            text: 1
                        },
                        children: [
                            {
                                title: 'Settings3',
                            },
                            {
                                title: 'Setting4',
                                children: [
                                    {
                                        title: 'Settings5',
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        title: 'Settings2',
                        des: {
                            color: '#ff6057',
                            text: 1
                        }
                    }
                ]
            },
            {
                logo: (props) => <Analytics20 {...props} />,
                title: 'Dashboard',
                toggle: true,
                des: {
                    color: '#ff6057',
                    text: 3
                }
            }
        ]
    },
    {
        title: 'Dashboard',
        toggle: true,
        children: [
            {
                logo: (props) => <Home20 {...props} />,
                title: 'Dashboard',
                link: '#'
            },
            {
                logo: (props) => <Activity20 {...props} />,
                title: 'Platform analytics',
                link: '#',
                des: {
                    color: '#D97706',
                    text: '!'
                },
                children: [
                    {
                        title: 'Settings',
                        des: {
                            color: '#ff6057',
                            text: 1
                        },
                        children: [
                            {
                                title: 'Settings3',
                            },
                            {
                                title: 'Setting4',
                                children: [
                                    {
                                        title: 'Settings5',
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        title: 'Settings2',
                        des: {
                            color: '#ff6057',
                            text: 1
                        }
                    }
                ]
            },
            {
                logo: (props) => <Analytics20 {...props} />,
                title: 'Dashboard',
                toggle: true,
                des: {
                    color: '#ff6057',
                    text: 3
                }
            }
        ]
    },
    {
        title: 'Dashboard',
        toggle: true,
        children: [
            {
                logo: (props) => <Home20 {...props} />,
                title: 'Dashboard',
                link: '#'
            },
            {
                logo: (props) => <Activity20 {...props} />,
                title: 'Platform analytics',
                link: '#',
                des: {
                    color: '#D97706',
                    text: '!'
                },
                children: [
                    {
                        title: 'Settings',
                        des: {
                            color: '#ff6057',
                            text: 1
                        },
                        children: [
                            {
                                title: 'Settings3',
                            },
                            {
                                title: 'Setting4',
                                children: [
                                    {
                                        title: 'Settings5',
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        title: 'Settings2',
                        des: {
                            color: '#ff6057',
                            text: 1
                        }
                    }
                ]
            },
            {
                logo: (props) => <Analytics20 {...props} />,
                title: 'Dashboard',
                toggle: true,
                des: {
                    color: '#ff6057',
                    text: 3
                }
            }
        ]
    },
    {
        title: 'Dashboard',
        toggle: true,
        children: [
            {
                logo: (props) => <Home20 {...props} />,
                title: 'Dashboard',
                link: '#'
            },
            {
                logo: (props) => <Activity20 {...props} />,
                title: 'Platform analytics',
                link: '#',
                des: {
                    color: '#D97706',
                    text: '!'
                },
                children: [
                    {
                        title: 'Settings',
                        des: {
                            color: '#ff6057',
                            text: 1
                        },
                        children: [
                            {
                                title: 'Settings3',
                            },
                            {
                                title: 'Setting4',
                                children: [
                                    {
                                        title: 'Settings5',
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        title: 'Settings2',
                        des: {
                            color: '#ff6057',
                            text: 1
                        }
                    }
                ]
            },
            {
                logo: (props) => <Analytics20 {...props} />,
                title: 'Dashboard',
                toggle: true,
                des: {
                    color: '#ff6057',
                    text: 3
                }
            }
        ]
    },
    {
        title: 'Dashboard',
        toggle: true,
        children: [
            {
                logo: (props) => <Home20 {...props} />,
                title: 'Dashboard',
                link: '#'
            },
            {
                logo: (props) => <Activity20 {...props} />,
                title: 'Platform analytics',
                link: '#',
                des: {
                    color: '#D97706',
                    text: '!'
                },
                children: [
                    {
                        title: 'Settings',
                        des: {
                            color: '#ff6057',
                            text: 1
                        },
                        children: [
                            {
                                title: 'Settings3',
                            },
                            {
                                title: 'Setting4',
                                children: [
                                    {
                                        title: 'Settings5',
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        title: 'Settings2',
                        des: {
                            color: '#ff6057',
                            text: 1
                        }
                    }
                ]
            },
            {
                logo: (props) => <Analytics20 {...props} />,
                title: 'Dashboard',
                toggle: true,
                des: {
                    color: '#ff6057',
                    text: 3
                }
            }
        ]
    },
    {
        title: 'Dashboard',
        toggle: true,
        children: [
            {
                logo: (props) => <Home20 {...props} />,
                title: 'Dashboard',
                link: '#'
            },
            {
                logo: (props) => <Activity20 {...props} />,
                title: 'Platform analytics',
                link: '#',
                des: {
                    color: '#D97706',
                    text: '!'
                },
                children: [
                    {
                        title: 'Settings',
                        des: {
                            color: '#ff6057',
                            text: 1
                        },
                        children: [
                            {
                                title: 'Settings3',
                            },
                            {
                                title: 'Setting4',
                                children: [
                                    {
                                        title: 'Settings5',
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        title: 'Settings2',
                        des: {
                            color: '#ff6057',
                            text: 1
                        }
                    }
                ]
            },
            {
                logo: (props) => <Analytics20 {...props} />,
                title: 'Dashboard',
                toggle: true,
                des: {
                    color: '#ff6057',
                    text: 3
                }
            }
        ]
    },
]
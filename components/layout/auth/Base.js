import React from 'react';

function Base({ children }) {
    return (
        <div className="container">
            <div className="h-full min-h-full w-full min-w-full flex items-center flex-row place-content-center">
                <div className="app-layout-divided-middle">
                    <div className="flex flex-row min-h-[660px]">
                        <div className="relative text-white hidden md:flex flex-1 bg-[#0f62fe]">
                            <div className="absolute bottom-0 right-0 left-0 top-0 opacity-50 z-0 app-bg-pattern-svg" />
                            <div className="flex items-center h-full z-10">
                                <div className="px-16">
                                    <div className="mb-4 text-[2.7rem] leading-[1.19] tracking-normal font-semibold">
                                        <div className="color-red-50">Welcome to Youpez</div>
                                        <div>the professional admin</div>
                                        <div>platform built with</div>
                                        <div>competence</div>
                                    </div>
                                    <p className="opacity-75 text-base font-normal leading-[1.5] max-w-[440px]" >
                                        Youpez helps you to focus on business logic with well-coded admin user interfaces and cutting edge best practices
                                    </p>
                                </div>
                                <div className="absolute top-0 right-0 left-0 z-10 px-16 py-8 flex justify-between">
                                    <img src="https://youpez.vercel.app/assets/img/logo/logo-big-white.png" className="app-logo--force-white w-[160px]"></img>
                                </div>
                                <div className="absolute bottom-0 right-0 left-0 z-10 px-16 py-8 flex justify-between text-sm">
                                    <div>©2020 Youpez</div>
                                    <div>
                                        <a className="mr-3 text-white cursor-pointer hover:underline">Privacy</a>
                                        <a className="mr-3 text-white cursor-pointer hover:underline">Security</a>
                                        <a className="mr-3 text-white cursor-pointer hover:underline">Legal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="px-16 py-16 app-bg-container bg-white place-content-center items-center flex-row flex flex-[1 1 450px] md:min-w-[450px] min-w-0 max-w-[450px]">
                            <div className="flex-1">
                                <div className="mb-8 md:hidden block">
                                    <div className="cursor-pointer">
                                        <img src="https://youpez.vercel.app/assets/img/logo/logo-big-black.png" className="w-[130px]" />
                                        <img src="https://youpez.vercel.app/assets/img/logo/logo-big-white.png" className="w-[130px]" />
                                    </div>
                                </div>
                                {children}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Base;
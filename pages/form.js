import React, { useState } from 'react';
import { Header, HeaderName, HeaderContainer, HeaderMenuButton, HeaderGlobalBar, HeaderGlobalAction, HeaderPanel, SideNav, SideNavHeader, SideNavItems, SideNavLink, Content } from 'carbon-components-react';
import { ArrowRight16, UserAvatar20 } from '@carbon/icons-react';

function form(props) {
    const [state, setState] = useState({
        switchCollapsed: false,
        toggle: false
    })
    const toggleSwitch = () => {
        setState(state => {
            return { ...state, switchCollapsed: !state.switchCollapsed }
        });
    }
    return (
        <div className="container">
            <HeaderContainer
                render={({ isSideNavExpanded, onClickSideNavExpand }) => (
                    <>
                        <Header aria-label="IBM Platform Name">
                            <HeaderMenuButton
                                aria-label="IBM Platform Name????"
                                onClick={onClickSideNavExpand}
                                isActive={isSideNavExpanded}
                            />
                            <HeaderName prefix="[Corp]">[Product]</HeaderName>
                            <HeaderGlobalBar>
                                <HeaderGlobalAction onClick={toggleSwitch}
                                    aria-label="IBM Platfora Name????"
                                >
                                    <UserAvatar20 />
                                </HeaderGlobalAction>
                            </HeaderGlobalBar>
                            <HeaderPanel
                                aria-label="Header Panel"
                                expanded={!state.switchCollapsed}

                            />
                            <SideNav
                                aria-label="Side navigation"
                                expanded={state.toggle}
                                isPersistent={false}

                            >
                                <SideNavItems>
                                    <SideNavLink to="/link1">
                                        One
                                    </SideNavLink>
                                    <SideNavLink to="/link2" isActive={true}>
                                        Two
                                    </SideNavLink>
                                    <SideNavLink to="/link3">
                                        Three
                                    </SideNavLink>
                                </SideNavItems>
                            </SideNav>
                        </Header>
                        <Content>
                            <div>Content</div>
                            <button onClick={() => setState({ ...state, toggle: !state.toggle })}>fsfs</button>
                        </Content>
                    </>
                )
                }
            />
        </div >
    );
}

export default form;
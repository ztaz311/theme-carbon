import { Menu24, Add20, Chat24, ChevronRight16, Help24, Home20, Notification24, Search24, Subtract20, User24, WatsonHealthContourFinding24 } from '@carbon/icons-react';
import { TooltipIcon, Modal, ComposedModal, ModalHeader, ModalBody, ModalFooter, ModalWrapper } from 'carbon-components-react';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import Menu from '../Menu';

const minSideBar = 30
function form2() {
	const [value, setValue] = useState(0)
	const [currentValue, setCurentValue] = useState(250)
	const [toggle, setToggle] = useState(false)
	const [checkHover, setCheckHover] = useState(false)
	const [isMobile, setIsMobile] = useState(null)
	const [open, setOpen] = useState(false);
	const [popover, setPopover] = useState({
		notification: false
	});
	useEffect(() => {
		if (isMobile == true) {
			setValue(0)
		} else {
			setValue(250)
		}
	}, [isMobile])
	useEffect(() => {
		let widthDefault = window.screen.width
		if (widthDefault > 375) {
			setIsMobile(false)
		} else {
			setIsMobile(true)
		}
		const resize = () => {
			setIsMobile(window.innerWidth <= 375);
		}
		window.addEventListener("resize", () => resize())
		return window.removeEventListener("resize", () => resize())

	}, [])
	var Direction;
	(function (Direction) {
		Direction["top"] = "top";
		Direction["bottom"] = "bottom";
		Direction["left"] = "left";
		Direction["right"] = "right";
	})(Direction || (Direction = {}));

	var useResizable = function useResizable(options, value, toggle, checkHover, isMobile) {
		var direction = options.direction,
			_options$maxSize = options.maxSize,
			maxSize = _options$maxSize === void 0 ? 10000 : _options$maxSize,
			_options$minSize = options.minSize,
			minSize = _options$minSize === void 0 ? 0 : _options$minSize;
		var container = React.useRef(null);
		var handle = React.useRef(null);
		var isResizingRef = React.useRef(false);

		var _useState = React.useState(false),
			isResizing = _useState[0],
			setResizing = _useState[1];

		var shouldUpdateResizing = React.useRef(false);
		React.useEffect(function () {
			var _handle$current;

			var handlePointerDown = function handlePointerDown() {
				isResizingRef.current = true;
				if (shouldUpdateResizing.current) setResizing(true);
			};

			(_handle$current = handle.current) == null ? void 0 : _handle$current.addEventListener('pointerdown', handlePointerDown);
		}, []);
		React.useEffect(function () {
			var getPanelHeight = function getPanelHeight(clientY, rect) {
				var position = rect[direction];
				var newHeight = rect.height;
				if (direction === Direction.top) newHeight -= clientY - position; else if (direction === Direction.bottom) newHeight -= position - clientY;
				if (newHeight < minSize) return minSize;
				if (newHeight > maxSize) return maxSize;
				return newHeight;
			};

			var getPanelWidth = function getPanelWidth(clientX, rect) {
				var position = rect[direction];
				var newWidth = rect.width;
				if (direction === Direction.left) newWidth -= clientX - position; else if (direction === Direction.right) newWidth -= position - clientX;
				if (newWidth < minSize) return minSize;
				if (newWidth > maxSize) return maxSize;
				return newWidth;
			};

			var handlePointerUp = function handlePointerUp(event) {
				isResizingRef.current = false;
				if (!toggle) {
					if (isMobile) {
						// setValue(0)
						// console.log('123', checkHover);
						// console.log('123', value);
						// if (value < 250 && checkHover) {
						// setValue(0)
						// }
					} else {
						if (value < 250) {
							setValue(250)
							setCurentValue(250)
						}
					}
				} else {
					if (value < 250 && checkHover) {
						setValue(250)
						setCurentValue(250)
					}
				}

				if (shouldUpdateResizing.current) {
					setResizing(false)
				};
			};

			var handlePointerMove = function handlePointerMove(event) {
				if (isResizingRef.current && container.current) {
					var rect = container.current.getBoundingClientRect();
					if (direction === Direction.top || direction === Direction.bottom) {
						container.current.style.height = getPanelHeight(event.clientY, rect) + "px";
					} else if (direction === Direction.left || direction === Direction.right) {
						setValue(getPanelWidth(event.clientX, rect))
						setCurentValue(getPanelWidth(event.clientX, rect))
						container.current.style.width = getPanelWidth(event.clientX, rect) + "px";
					}
				}
			};

			window.addEventListener('pointerup', handlePointerUp);
			window.addEventListener('pointermove', handlePointerMove);
			return function () {
				window.removeEventListener('pointerup', handlePointerUp);
				window.removeEventListener('pointermove', handlePointerMove);
			};
		}, [direction, maxSize, minSize, value, toggle]);
		return React.useMemo(function () {
			var refs = {
				container: container,
				handle: handle
			};
			Object.defineProperty(refs, 'isResizing', {
				get: function get() {
					shouldUpdateResizing.current = true;
					return isResizing;
				},
				enumerable: true
			});
			return refs;
		}, [isResizing]);
	};

	function styleInject(css, ref) {
		if (ref === void 0) ref = {};
		var insertAt = ref.insertAt;

		if (!css || typeof document === 'undefined') { return; }

		var head = document.head || document.getElementsByTagName('head')[0];
		var style = document.createElement('style');
		style.type = 'text/css';

		if (insertAt === 'top') {
			if (head.firstChild) {
				head.insertBefore(style, head.firstChild);
			} else {
				head.appendChild(style);
			}
		} else {
			head.appendChild(style);
		}

		if (style.styleSheet) {
			style.styleSheet.cssText = css;
		} else {
			style.appendChild(document.createTextNode(css));
		}
	}
	var css_248z = ".Resizable-module_handle__1mp2v{position:absolute;user-select:none}.Resizable-module_handle__1mp2v[data-direction=bottom],.Resizable-module_handle__1mp2v[data-direction=top]{height:10px;width:100%;cursor:row-resize}.Resizable-module_handle__1mp2v[data-direction=left],.Resizable-module_handle__1mp2v[data-direction=right]{height:100%;width:10px;cursor:col-resize}.Resizable-module_handle__1mp2v[data-direction=top]{top:-5px}.Resizable-module_handle__1mp2v[data-direction=bottom]{bottom:-5px}.Resizable-module_handle__1mp2v[data-direction=left]{left:-5px}.Resizable-module_handle__1mp2v[data-direction=right]{right:-5px}";
	var styles = { "root": "Resizable-sidebar", "handle": "Resizable-module_handle__1mp2v" };

	var Resizable = function Resizable(props) {
		var className = props.className,
			_props$classes = props.classes,
			classes = _props$classes === void 0 ? {} : _props$classes,
			children = props.children,
			direction = props.direction,
			maxSize = props.maxSize,
			minSize = props.minSize,
			_props$resizable = props.resizable,
			resizable = _props$resizable === void 0 ? true : _props$resizable,
			_props$style = props.style,
			style = _props$style === void 0 ? {} : _props$style;

		var _useResizable = useResizable({
			direction: direction,
			maxSize: maxSize,
			minSize: minSize
		}, props.value, props.toggle, props.checkHover, props.isMobile),
			container = _useResizable.container,
			handle = _useResizable.handle;
		let onEvent = {}
		if (props.toggle) {
			onEvent = {
				onMouseOver: () => { container.current.style.width = props.currentValue + 'px', setValue(props.currentValue), setCheckHover(true) },
				onMouseLeave: () => { container.current.style.width = isMobile ? 0 + 'px' : minSideBar + 'px', setValue(isMobile ? 0 : minSideBar), setCheckHover(false) }
			}
		}

		return React.createElement("div", {
			className: [styles.root, classes.root, className].filter(Boolean).join(' '),
			"data-testid": "resizable",
			ref: container,
			style: { width: props.value },
			id: "toggleSidebar",
			...onEvent
		}, resizable && React.createElement("span", {
			className: [styles.handle, classes.handle].filter(Boolean).join(' '),
			"data-direction": direction,
			"data-testid": "handle",
			ref: handle,
			id: "dragSidebar",
		}), children);
	};



	useEffect(() => {
		styleInject(css_248z);
	}, [])


	const onSearch = () => {
		setOpen(!open)
	}

	const renderChildren2 = (item) => {
		const refChildren2 = useRef()
		const [takeIconChild2, setTakeIconChild2] = useState(false)
		const toggleChild2 = () => {
			setTakeIconChild2(refChildren2.current.classList.contains('collapsed'))
			refChildren2.current.classList.toggle('collapsed');
		}

		return (
			<div className="ml-[30px] tracking-wide">
				<div className="pl-3 py-1 min-h-[28px] flex flex-row justify-between items-center hover:bg-[#1f1f23] hover:text-[#eaeaeb] hover:rounded-lg" onClick={toggleChild2}>
					{item.title}
					{item?.children && item?.children?.length > 0 &&
						<ChevronRight16 className="mr-5" style={{ transform: takeIconChild2 ? 'rotate(90deg)' : null }} />}
				</div>
				<div className="section collapsible collapsed" ref={refChildren2}>
					{
						item?.children && item?.children?.length > 0 && item?.children.map((item, index) => {
							return (
								<React.Fragment key={'chil2' + index}>
									{renderChildren2(item)}
								</React.Fragment>
							)
						})
					}
				</div>
			</div>
		)
	}




	const renderChildren = (item) => {
		const refChildren = useRef()
		const [takeIconChild, setTakeIconChild] = useState(false)
		const toggleChild = () => {
			setTakeIconChild(refChildren.current.classList.contains('collapsed'))
			refChildren.current.classList.toggle('collapsed');
		}

		return (
			<div className="leading-4 text-sm flex flex-col ">
				<div className="pl-3 flex flex-row items-center justify-between flex-1 min-h-[38px] hover:bg-[#1f1f23] hover:text-[#eaeaeb] hover:rounded-lg" onClick={toggleChild}>
					<div className="flex flex-row items-center">
						{/* <Activity20 description="Dashboard" className="mr-2" /> */}
						{item.logo({ className: 'mr-2', description: 'Dashboard' })}
						{item.title}
					</div>
					{/* <div className="text-red-500 mr-1">!</div> */}
					<div className="mr-5 flex flex-row">
						{item?.des && <div className="text-[9px] bg-[#413131] px-[2px] py-[1px] rounded-2xl text-center min-w-[18px] inline-block font-bold" style={{ color: item.des.color || 'white' }}>{item.des.text}</div>}
						{item?.children && item.children.length > 0 && <ChevronRight16 className="ml-1" style={{ transform: takeIconChild ? 'rotate(90deg)' : null }} />}
					</div>
					{/* {item?.children && item.children.length > 0 ?  : <span className="" />} */}
				</div>
				<div className="section collapsible collapsed" ref={refChildren}>
					{
						item?.children !== undefined && item?.children.map((item, index) => {
							return (
								<React.Fragment key={'children' + index}>
									{renderChildren2(item)}
								</React.Fragment>
							)
						})
					}
				</div>
			</div>
		)
	}

	const renderSideBarItem = (item) => {
		const refItem = useRef()
		const [takeIcon, setTakeIcon] = useState(true)

		const toggleSidebar = () => {
			setTakeIcon(refItem.current.classList.contains('collapsed'))
			refItem.current.classList.toggle('collapsed');
		}

		return (
			<div className="section text-xs mt-1 ml-5 tracking-wide text-[#808080] cursor-pointer">
				<div className="flex flex-row justify-between items-center mb-3 mr-5 overflow-hidden" onClick={() => toggleSidebar()}>
					<div className="uppercase">{item.title}</div>
					{item?.children && item.children.length > 0 &&
						<div>
							{takeIcon ? <Subtract20 /> : <Add20 />}
						</div>}
				</div>
				<div className="section collapsible" ref={refItem}>
					{
						item?.children !== undefined && item?.children.map((item, index) => {
							return (
								<React.Fragment key={'Child' + index}>
									{renderChildren(item)}
								</React.Fragment>
							)
						})
					}
				</div>
			</div>
		)
	}






	const MenuLeft = useCallback((value, toggle, currentValue, checkHover, isMobile) => {
		return (
			<>
				<Resizable direction="right"
					minSize={30}
					value={value}
					toggle={toggle}
					currentValue={currentValue}
					checkHover={checkHover}
					isMobile={isMobile}
					//  minSize={state.toggle ? state.value : state.value2} 
					className={`bg-[#0f0f12] overflow-hidden z-20 ${toggle ? 'absolute top-0 bottom-0 md:left-[59px] left-0' : 'md:relative absolute top-0 bottom-0 left-0 '}`}>
					<div className="h-[65px] bg-[#27272B] pl-4 pr-5 flex flex-row items-center justify-between">
						<img src="https://youpez.vercel.app/assets/img/logo/logo-big-white.png" className="w-[145px]" />
						{/* <RadioButton className="border-red-400" id="checked" onChange={() => console.log('123')} /> */}

						{isMobile ?
							<button className="bx--tooltip__trigger overflow-hidden" onClick={() => setToggle(true)}>
								<svg xmlns="http://www.w3.org/2000/svg" focusable="false" preserveAspectRatio="xMidYMid meet" aria-hidden="true" width="16" height="16" viewBox="0 0 32 32" className="ng-star-inserted "><path d="M16,2A14,14,0,1,0,30,16,14,14,0,0,0,16,2Zm0,26A12,12,0,1,1,28,16,12,12,0,0,1,16,28Z"></path><path d="M16,10a6,6,0,1,0,6,6A6,6,0,0,0,16,10Z"></path></svg>
							</button>
							:
							!toggle ?
								<button className="bx--tooltip__trigger overflow-hidden" onClick={() => setToggle(true)}>
									<svg xmlns="http://www.w3.org/2000/svg" focusable="false" preserveAspectRatio="xMidYMid meet" aria-hidden="true" width="16" height="16" viewBox="0 0 32 32" className="ng-star-inserted "><path d="M16,2A14,14,0,1,0,30,16,14,14,0,0,0,16,2Zm0,26A12,12,0,1,1,28,16,12,12,0,0,1,16,28Z"></path><path d="M16,10a6,6,0,1,0,6,6A6,6,0,0,0,16,10Z"></path></svg>
								</button>
								:
								<button className="bx--tooltip__trigger overflow-hidden" onClick={() => setToggle(false)}>
									<svg xmlns="http://www.w3.org/2000/svg" focusable="false" preserveAspectRatio="xMidYMid meet" aria-hidden="true" width="16" height="16" viewBox="0 0 32 32" className="ng-star-inserted"><path d="M16,2A14,14,0,1,0,30,16,14,14,0,0,0,16,2Zm0,26A12,12,0,1,1,28,16,12,12,0,0,1,16,28Z"></path></svg>
								</button>
						}
					</div>

					<div className="text-white scrollbarBlack overflow-y-scroll" style={{ height: 'calc(100vh - 65px)' }}>
						<div className="mt-4">
							{Menu.map((item, index) => {
								return (
									<React.Fragment key={'SideBar' + index}>
										{renderSideBarItem(item)}
									</React.Fragment>
								)
							})}
						</div>
					</div>
				</Resizable>

			</>
		)
	}, []);

	function useOuterClick(callback) {

		const callbackRef = useRef(); // initialize mutable ref, which stores callback
		const innerRef = useRef(); // returned to client, who marks "border" element

		// update cb on each render, so second useEffect has access to current value 
		useEffect(() => { callbackRef.current = callback; });

		useEffect(() => {
			document.addEventListener("click", handleClick);
			return () => document.removeEventListener("click", handleClick);
			function handleClick(e) {
				if (innerRef.current && callbackRef.current &&
					!innerRef.current.contains(e.target)
				) callbackRef.current(e);
			}
		}, []); // no dependencies -> stable click listener

		return innerRef; // convenience for client (doesn't need to init ref himself) 
	}


	const innerRef = useOuterClick(ev => setPopover({ notification: false }));

	return (
		<div className="min-w-screen min-h-screen">
			<div className="flex flex-row ">

				{/* Header mobile */}
				<div className={`flex items-center h-12 w-full bg-black fixed top-0 md:invisible visible ${value > 0 ? 'z-20' : 'z-30'}`}>
					<Menu24 description="Menu" fill="#b7b7b8" className=" ml-3 -p-2" onClick={() => setValue(currentValue)} />
					<img src="https://youpez.vercel.app/assets/img/logo/logo-big-white.png" className="w-[120px] ml-4" />

					<div className="flex flex-row justify-end items-center flex-1 px-2">
						<Search24 description="Search24" fill="#b7b7b8" className="cursor-pointer before:border-black after:border-black mr-4" onClick={onSearch} />
						<WatsonHealthContourFinding24 description="WatsonHealthContourFinding24" fill="#b7b7b8" className="cursor-pointer mr-4" />
						<User24 description="User24" fill="#b7b7b8" />
					</div>
				</div>
				{/*  */}
				{/* left side */}
				<div className="flex h-screen z-20">
					<div className="w-[59px] bg-[#19191e] flex flex-col items-center justify-between p-5 overflow-visible md:visible invisible z-30">
						<div>
							<div className="cursor-pointer w-10 h-10 rounded flex justify-center items-center hover:bg-[#27272b] mb-3" onClick={onSearch} >
								<TooltipIcon
									id="Search24"
									direction="right"
									tooltipText="fss"
									className="cursor-pointer">
									<Search24 description="Search24" fill="#b7b7b8" className="cursor-pointer before:border-black after:border-black" />
								</TooltipIcon>
							</div>
							<div className="cursor-pointer w-10 h-10 rounded flex justify-center items-center hover:bg-[#27272b] mb-2 relative" ref={innerRef} onClick={() => setPopover({ ...popover, notification: !popover.notification })}>
								<TooltipIcon
									id="Search24"
									direction="right"
									tooltipText="fss"
									className="cursor-pointer">
									<Notification24 description="Notification24" fill="#b7b7b8" className="cursor-pointer" />
								</TooltipIcon>
								{popover.notification && <div className="absolute top-[40px] left-0 z-10 max-h-[300px] max-w-[300px] bg-white p-4 text-black">
									<div className="flex flex-row justify-between items-center">
										<span>Notifications</span>
										<span>+12</span>
									</div>
									<div>
										<div>
											<span>Bug</span>Faifsfsfnsjfnsfsfnsfsfs....
										</div>
										<div>
											<span>Bug</span>Faifsfsfnsjfnsfsfnsfsfs....
										</div>
										<div>
											<span>Bug</span>Faifsfsfnsjfnsfsfnsfsfs....
										</div>
										<div>
											<span>Bug</span>Faifsfsfnsjfnsfsfnsfsfs....
										</div>
										<div>
											<span>Bug</span>Faifsfsfnsjfnsfsfnsfsfs....
										</div>
										<div>
											<span>Bug</span>Faifsfsfnsjfnsfsfnsfsfs....
										</div>
									</div>
								</div>}
							</div>
							<div className="cursor-pointer w-10 h-10 rounded flex justify-center items-center hover:bg-[#27272b] mb-2">
								<TooltipIcon
									id="Search24"
									direction="right"
									tooltipText="fss"
									className="cursor-pointer">
									<Chat24 description="Chat24" fill="#b7b7b8" className="cursor-pointer" />
								</TooltipIcon>
							</div>
							<div className="cursor-pointer w-10 h-10 rounded flex justify-center items-center hover:bg-[#27272b] mb-2">
								<TooltipIcon
									id="Search24"
									direction="right"
									tooltipText="fss"
									className="cursor-pointer">
									<WatsonHealthContourFinding24 description="WatsonHealthContourFinding24" fill="#b7b7b8" className="cursor-pointer" />
								</TooltipIcon>
							</div>
						</div>
						<div>
							<div className="cursor-pointer w-10 h-10 rounded flex justify-center items-center hover:bg-[#27272b]">
								<TooltipIcon
									id="Search24"
									direction="right"
									tooltipText="fss"
									className="cursor-pointer">
									<Help24 description="Help24" fill="#b7b7b8" />                                </TooltipIcon>
							</div>
							<div className="cursor-pointer w-10 h-10 rounded flex justify-center items-center hover:bg-[#27272b] mt-2 -mb-2">
								<TooltipIcon
									id="Search24"
									direction="right"
									tooltipText="fss"
									className="cursor-pointer">
									<User24 description="User24" fill="#b7b7b8" />
								</TooltipIcon>
							</div>
						</div>
					</div>

					{MenuLeft(value, toggle, currentValue, checkHover, isMobile)}

					{console.log('value', value)}
					{isMobile && value > 0 &&
						<div
							onClick={() => setValue(0)}
							className="absolute top-0 right-0 bottom-0 left-[100px] bg-white opacity-60 z-10 md:invisible visible" />}
				</div>


				<ComposedModal open={open}
					size="sm"
					onClose={() => setOpen(false)}
					containerClassName="rounded w-[600px]"
				>
					<ModalHeader className="border-b p-0 border-[#e0e0e0] border-solid mb-0"
						//  closeClassName="invisible"
						style={{ padding: 0, marginBottom: 0 }}>
						<Search24 description="Search24" fill="#161616" className="w-[34px] h-[34px] absolute left-4 top-4" />
						<input type="text" className="bg-transparent py-[0.8rem] px-[3.75rem] w-full text-[1.8rem] leading-[1.8rem] outline-none" />
					</ModalHeader>
					<ModalBody aria-label="ModalSearch" style={{ padding: 0, marginBottom: 5, maxHeight: 300, overflowY: 'hidden' }}>
						<div className="flex md:flex-row flex-col">
							<div className="md:w-2/5 md:max-h-[300px] max-h-[150px] w-full flex-col border-gray-300 border-r-1 border-solid modal-content overflow-y-scroll">
								<div>
									<div className="w-full break-all bg-[#ebebeb] text-[#161616] px-4 py-2 text-xs font-semibold">
										TOP HITS
									</div>
									<div className="w-full text-sm">
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfs fdf dfgd gđ gd gd fsfsfsssfsg fsfsf</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfsssssssggggsdf</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfs</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfs</div>
									</div>
								</div>
								<div>
									<div className="w-full bg-[#ebebeb] text-[#161616] px-4 py-2 text-xs font-semibold">
										TOP HITS
									</div>
									<div className="w-full text-sm">
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all bg-[#f4f4f4]">Kube fdsfsfsfsfs</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfs</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfs</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfs</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfs</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfs</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfs</div>
										<div className="px-4  border-gray-300 border-b-1 border-solid  break-all">Kube fdsfsfsfsfs</div>
									</div>
								</div>

							</div>
							<div className="md:w-3/5 md:max-h-[300px] max-h-[300px] w-full p-6 overflow-y-scroll">
								<div className="flex flex-row">
									<div className="rounded bg-[#a6ebba] mr-3 p-2 text-[#24a148] font-bold text-sm tracking-[-0.5px]">
										<span>K8S</span>
									</div>
									<div>
										<div className="text-xl font-normal leading-5">Kubernetes workload</div>
										<div className="text-[#24a148] text-xs">(GKS) status - OK</div>
									</div>
								</div>
								<div>
									This is the production cluster. To access Dashboard from your local workstation you must create a secure channel to your Kubernetes cluster.
								</div>

							</div>
						</div>
					</ModalBody>
				</ComposedModal>


				<div style={isMobile === true ? { position: 'absolute', top: 48, left: 0, right: 0, bottom: 0 } : { marginLeft: toggle ? 30 : 0 }} className="break-all h-screen overflow-y-scroll w-full bg-white px-3">
					2fsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdf
					<div>
						2fsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdf
					</div>
					<div className="mt-96">
						2fsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdf
					</div>
					<div className="mt-96">
						2fsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdf
					</div>
					<div className="mt-96">
						2fsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdf
					</div>
					<div className="mt-96">
						2fsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdffsfsfsgdgdgdgdgdgdgdf
					</div>
				</div>

				{/*  */}
			</div>
		</div >
	);
}

export default form2;
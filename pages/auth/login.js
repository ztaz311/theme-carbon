import React, { useState } from 'react';
import Base from 'components/layout/auth/Base'
import { Button, TextInput, Checkbox } from 'carbon-components-react';
import { ArrowRight16 } from '@carbon/icons-react';

function login(props) {
    const [input, setInut] = useState({
        email: '',
        password: ''
    })
    const onChangeInput = (e) => {
        setInut({
            ...input,
            [e.target.id]: e.target.value
        })
    }

    const [checked, setChecked] = useState(false)
    return (
        <Base>
            <h1 className="text-[2rem] font-semibold leading-[1.25] mb-1">
                Sign in
            </h1>
            <div className="mb-8 text-base font-normal leading-[1.5]">
                Don't have an account?
                <a className="bx--link hover:underline" href="./signup">&nbsp;Sign up</a>
            </div>
            <form onSubmit={(e) => e.preventDefault()}>
                <div >
                    <div className="mb-[1.1rem]">
                        <TextInput
                            id="email"
                            labelText="Email address"
                            placeholder="Email address"
                            type='text'
                            value={input.email}
                            onChange={onChangeInput}
                            invalidText="This field is required"
                            invalid={input.email === ''}
                        />
                    </div>
                    <div className="mb-[1.1rem]">
                        <TextInput.PasswordInput
                            labelText="Password"
                            placeholder="Password"
                            type='password'
                            value={input.password}
                            onChange={onChangeInput}
                            id="password"
                            invalidText="This field is required"
                            invalid={input.password === ''}
                        />
                    </div>
                    <div className="mb-[1rem] mt-[-.5rem]">
                        <div className="flex justify-between items-center">
                            <Checkbox id="remember" checked={checked} onChange={setChecked} labelText={`Remember me?`} />
                            <a href="#" className="bx--link">Forgot password?</a>
                        </div>
                    </div>
                    <div className="app-form__submit">
                        <Button type="submit" size="sm" iconDescription="Add" renderIcon={ArrowRight16} className="w-full min-w-full max-h-full block min-h-full">
                            <div className="absolute left-[41%]">Sign In</div>
                        </Button>
                    </div>
                </div>
            </form>
            <div className="mt-8 mb-8 text-center flex items-center after:bg-gray-400 after:flex-1 after:h-[1px] after:ml-1
            before:bg-gray-400 before:flex-1 before:h-[1px] before:mr-1
            ">
                or Sign In with
            </div>
            <div className="flex flex-row box-border">
                <div className="mr-1 flex-1 max-w-[50%]">
                    <Button size="md" kind="tertiary"
                        renderIcon={() => <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" className="bx--btn__icon">
                            <path d="M12.24 10.285V14.4h6.806c-.275 1.765-2.056 5.174-6.806 5.174-4.095 0-7.439-3.389-7.439-7.574s3.345-7.574 7.439-7.574c2.33 0 3.891.989 4.785 1.849l3.254-3.138C18.189 1.186 15.479 0 12.24 0c-6.635 0-12 5.365-12 12s5.365 12 12 12c6.926 0 11.52-4.869 11.52-11.726 0-.788-.085-1.39-.189-1.989H12.24z"></path></svg>
                        }
                        className="w-full">
                        Google
                    </Button>
                </div>
                <div className="mr-1 flex-1 max-w-[50%]">
                    <Button size="md" kind="tertiary"
                        renderIcon={() => <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" className="bx--btn__icon">
                            <path d="M23.9981 11.9991C23.9981 5.37216 18.626 0 11.9991 0C5.37216 0 0 5.37216 0 11.9991C0 17.9882 4.38789 22.9522 10.1242 23.8524V15.4676H7.07758V11.9991H10.1242V9.35553C10.1242 6.34826 11.9156 4.68714 14.6564 4.68714C15.9692 4.68714 17.3424 4.92149 17.3424 4.92149V7.87439H15.8294C14.3388 7.87439 13.8739 8.79933 13.8739 9.74824V11.9991H17.2018L16.6698 15.4676H13.8739V23.8524C19.6103 22.9522 23.9981 17.9882 23.9981 11.9991Z"></path></svg>}
                        className="w-full">
                        Facebook
                    </Button>
                </div>
            </div>
        </Base>
    );
}

export default login;
import React from 'react';
import { Button, TextInput, Link } from 'carbon-components-react';
import { CheckmarkOutline16 } from '@carbon/icons-react';
function lock() {
    return (
        <div className="flex justify-center items-center w-screen h-screen">
            <div className="bg-image-lock absolute top-0 bottom-0 left-0 right-0 opacity-30" />
            <div className="w-[300px] h-[381px] rounded flex flex-col justify-center  bg-white z-10 shadow-3xl p-6">
                <img src="https://youpez.vercel.app/assets/img/logo/logo-big-black.png" className="w-[140px] mb-6" />
                <div className="font-bold text-xl">Unlock your session</div>
                <div className="mb-4 text-sm">Your session is locked due to inactivity</div>
                <div className="flex flex-row items-center py-3">
                    <img src="https://youpez.vercel.app/assets/img/avatar/avatar2.jpg" className="w-10 h-10 rounded-full mr-2" />
                    <div className="flex-col mr-4">
                        <div className="text-xs">Signed in as</div>
                        <div className="text-base font-semibold">Tuấn Anh</div>
                    </div>
                </div>
                <TextInput
                    id="password"
                    labelText="Password"
                    // placeholder="Password"
                    type='text'
                    // value={input.email}
                    // onChange={onChangeInput}
                    invalidText="This field is required"
                // invalid={input.email === ''}
                />
                <Button type="submit" size="md" iconDescription="Submit" renderIcon={CheckmarkOutline16} className="">
                    <div className="absolute left-[30%]">Unlock session</div>
                </Button>
                <Link className="text-center pt-5 self-center">I am not David Robson</Link>
            </div>
        </div>
    );
}

export default lock;
import React from 'react';
import Base from 'components/layout/auth/Base'
import { Button, TextInput, Checkbox } from 'carbon-components-react';
import { ArrowRight16 } from '@carbon/icons-react';

function signup() {
    return (
        <Base>
            <h1 className="text-[2rem] font-semibold leading-[1.25] mb-1">
                Sign up
            </h1>
            <div className="mb-8 text-base font-normal leading-[1.5]">
                Already have an account?
                <a className="bx--link hover:underline" href="./login">&nbsp;Sign in</a>
            </div>
            <form >
                <div className="m-[0 -.55rem] flex-row box-border flex">
                    <div className="flex flex-1 box-border max-w-[50%]" >
                        <div className="mb-[1.1rem] pr-[.55rem]">
                            <TextInput
                                id="email"
                                labelText="Email address"
                                placeholder="Email address"
                                type='text'
                            />
                        </div>
                    </div>
                    <div className="flex flex-1 box-border max-w-[50%]" >
                        <div className="mb-[1.1rem] pl-[.55rem]">
                            <TextInput
                                id="email"
                                labelText="Email address"
                                placeholder="Email address"
                                type='text'
                            />
                        </div>
                    </div>
                </div>
                <div className="app-form__item">
                    <TextInput
                        id="email"
                        labelText="Email address"
                        placeholder="Email address"
                        type='text'
                    />
                </div>
                <div className="app-form__item mt-4">
                    <TextInput
                        id="email"
                        labelText="Email address"
                        placeholder="Email address"
                        type='text'
                    />
                </div>
                <div className="app-form__item app-form__item--opts">
                    <div className="flex justify-between items-center">
                        {/* <div formcontrolname="condition" className="bx--checkbox-wrapper bx--form-item "><div className="bx--form-item bx--checkbox-wrapper">
                            <input type="checkbox" className="bx--checkbox" id="checkbox-0_input" value="undefined" name="undefined" aria-checked={true} />
                            <label className="bx--checkbox-label" htmlFor="checkbox-0_input" aria-label="">
                                <span className="bx--checkbox-label-text"> I have read and accept
                                    <a className="app-link">terms &amp; conditions</a>
                                </span>
                            </label>
                        </div>
                        </div> */}
                        <Checkbox id="accept"
                            className="mt-4 mb-5"
                            labelText={
                                <div className=""> I have read and accept
                                    <a className="ml-1 hover:underline" href="./login">terms conditions</a>
                                </div>}
                        />

                    </div>
                </div>
                <div className="app-form__submit">
                    {/* <button type="submit"
                        ibmbutton="primary" size="field"
                        className="app-ibm-btn-full bx--btn bx--btn--primary bx--btn--field"> Create your account
                        <svg size="20" className="bx--btn__icon" xmlns="http://www.w3.org/2000/svg" focusable="false" preserveAspectRatio="xMidYMid meet" aria-hidden="true" width="20" height="20" viewBox="0 0 20 20"><path d="M11.8 2.8L10.8 3.8 16.2 9.3 1 9.3 1 10.7 16.2 10.7 10.8 16.2 11.8 17.2 19 10z"></path></svg>
                    </button> */}
                    <Button type="submit" size="md" iconDescription="Add" renderIcon={ArrowRight16} className="w-full min-w-full max-h-full block min-h-full">
                        <div className="w-full text-center ml-7">Create your account</div>
                    </Button>
                </div>
            </form>



        </Base>
    );
}

export default signup;